positionImagesA = [
    '1A',
    '2A',
    '3A',
    '4A',
    '5A',
    '6A',
    '7A',
    '8A',
    '9A',
    '10A',
    '11A',
    '12A',
]

positionImagesB = [
    '1B',
    '2B',
    '3B',
    '4B',
    '5B',
    '6B',
    '7B',
    '8B',
    '9B',
    '10B',
    '11B',
    '12B',
]

resultImages = [
    '1R',
    '2R',
    '3R',
    '4R',
    '5R',
    '6R',
    '7R',
    '8R',
    '9R',
    '10R',
    '11R',
    '12R',
]

resultText = [
    'TextRes1',
    'TextRes2',
    'TextRes3',
    'TextRes4',
    'TextRes5',
    'TextRes6',
    'TextRes7',
    'TextRes8',
    'TextRes9',
    'TextRes10',
    'TextRes11',
    'TextRes12',
    'TextRes13',
]

imagesSeg = [
    'OP',
    'SG'
]

allCodes = [
    resultImages,
    positionImagesA,
    positionImagesB
]