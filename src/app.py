import PySimpleGUI as sg
import paho.mqtt.client as mqtt
import bluetooth
from threading import Thread
import json
from PIL import Image, ImageTk
from Model.elements import positionImagesA, positionImagesB, resultImages, resultText, imagesSeg

hiddenimports=['PIL', 'PIL._imagingtk', 'PIL._tkinter_finder','PIL.Image','PIL.ImageTk']

"""
    Dashboard using blocks of information.
    Copyright 2020 PySimpleGUI.org
"""

topic = "/python/mqtt"
client_id = "test/py/simple/gui"

codes = []

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("$SYS/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    window['CBox1A'].update(True)
    window['TextRes1'].update("423423")

def connectMQTT():
    client = mqtt.Client()
    client.connect("broker.hivemq.com", 1883, 60)
    client.subscribe(topic)
    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_start()


CODE_MAP_CHAR = {
    'KEY_MINUS': "-",
    'KEY_SPACE': " ",    
    'KEY_U': "U",
    'KEY_W': "W",
    'KEY_BACKSLASH': "\\",
    'KEY_GRAVE': "`",
    'KEY_NUMERIC_STAR': "*",
    'KEY_NUMERIC_3': "3",
    'KEY_NUMERIC_2': "2",
    'KEY_NUMERIC_5': "5",
    'KEY_NUMERIC_4': "4",
    'KEY_NUMERIC_7': "7",
    'KEY_NUMERIC_6': "6",
    'KEY_NUMERIC_9': "9",
    'KEY_NUMERIC_8': "8",
    'KEY_NUMERIC_1': "1",
    'KEY_NUMERIC_0': "0",
    'KEY_E': "E",
    'KEY_D': "D",
    'KEY_G': "G",
    'KEY_F': "F",
    'KEY_A': "A",
    'KEY_C': "C",
    'KEY_B': "B",
    'KEY_M': "M",
    'KEY_L': "L",
    'KEY_O': "O",
    'KEY_N': "N",
    'KEY_I': "I",
    'KEY_H': "H",
    'KEY_K': "K",
    'KEY_J': "J",
    'KEY_Q': "Q",
    'KEY_P': "P",
    'KEY_S': "S",
    'KEY_X': "X",
    'KEY_Z': "Z",
    'KEY_q': "q",
    'KEY_w': "w",
    'KEY_e': "e",
    'KEY_r': "r",
    'KEY_t': "t",
    'KEY_z': "z",
    'KEY_u': "u",
    'KEY_i': "i",
    'KEY_o': "o",
    'KEY_p': "p",
    'KEY_a': "a",
    'KEY_s': "s",
    'KEY_d': "d",
    'KEY_f': "f",
    'KEY_g': "g",
    'KEY_h': "h",
    'KEY_j': "k",
    'KEY_l': "l",
    'KEY_y': "y",
    'KEY_x': "x",
    'KEY_c': "c",
    'KEY_v': "v",
    'KEY_b': "b",
    'KEY_n': "n",
    'KEY_m': "m",
    'KEY_KP4': "4",
    'KEY_KP5': "5",
    'KEY_KP6': "6",
    'KEY_KP7': "7",
    'KEY_KP0': "0",
    'KEY_KP1': "1",
    'KEY_KP2': "2",
    'KEY_KP3': "3",
    'KEY_KP8': "8",
    'KEY_KP9': "9",
    'KEY_5': "5",
    'KEY_4': "4",
    'KEY_7': "7",
    'KEY_6': "6",
    'KEY_1': "1",
    'KEY_0': "0",
    'KEY_3': "3",
    'KEY_2': "2",
    'KEY_9': "9",
    'KEY_8': "8",
    'KEY_LEFTBRACE': "[",
    'KEY_RIGHTBRACE': "]",    
    'KEY_COMMA': ",",
    'KEY_EQUAL': "=",    
    'KEY_SEMICOLON': ";",
    'KEY_APOSTROPHE': "'",
    'KEY_T': "T",
    'KEY_V': "V",
    'KEY_R': "R",
    'KEY_Y': "Y",
    'KEY_TAB': "\t",
    'KEY_DOT': ".",
    'KEY_SLASH': "/",
}
    
scancodes = {
    # Scancode: ASCIICode
    0: None, 1: u'ESC', 2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8',
    10: u'9', 11: u'0', 12: u'-', 13: u'=', 14: u'BKSP', 15: u'TAB', 16: u'Q', 17: u'W', 18: u'E', 19: u'R',
    20: u'T', 21: u'Y', 22: u'U', 23: u'I', 24: u'O', 25: u'P', 26: u'[', 27: u']', 28: u'CRLF', 29: u'LCTRL',
    30: u'A', 31: u'S', 32: u'D', 33: u'F', 34: u'G', 35: u'H', 36: u'J', 37: u'K', 38: u'L', 39: u';',
    40: u'"', 41: u'`', 42: u'LSHFT', 43: u'\\', 44: u'Z', 45: u'X', 46: u'C', 47: u'V', 48: u'B', 49: u'N',
    50: u'M', 51: u',', 52: u'.', 53: u'/', 54: u'RSHFT', 56: u'LALT', 100: u'RALT'
}

def parse_key_to_char(val):
    return CODE_MAP_CHAR[val] if val in CODE_MAP_CHAR else ""

theme_dict = {'BACKGROUND': '#2B475D',
                'TEXT': '#FFFFFF',
                'INPUT': '#F2EFE8',
                'TEXT_INPUT': '#000000',
                'SCROLL': '#F2EFE8',
                'BUTTON': ('#000000', '#C2D4D8'),
                'PROGRESS': ('#FFFFFF', '#C7D5E0'),
                'BORDER': 1,'SLIDER_DEPTH': 0, 'PROGRESS_DEPTH': 0}

# sg.theme_add_new('Dashboard', theme_dict)     # if using 4.20.0.1+
sg.LOOK_AND_FEEL_TABLE['Dashboard'] = theme_dict
sg.theme('Dashboard')

def setLedRedAll(window,image):
    for imagePosA in positionImagesA:
        window[imagePosA].update(data=image)
        codes.append(imagePosA)
    for imagePosB in positionImagesB:
        window[imagePosB].update(data=image)
        codes.append(imagePosB)
    for imageRes in resultImages:
        window[imageRes].update(data=image)
        codes.append(imageRes)
    for imageSeg in imagesSeg:
        window[imageSeg].update(data=image)
        codes.append(imageSeg)
    # print(codes)
    
def updateStatus(window,ledGreen,element):
    window[element].update(data=ledGreen)

def analyzeCommand(command):
    print("COMANDO = "+command)

def setLedGreenAll(window,ledGreen):
    for imagePosA in positionImagesA:
        window[imagePosA].update(data=ledGreen)
    for imagePosB in positionImagesB:
        window[imagePosB].update(data=ledGreen)
    for imageRes in resultImages:
        window[imageRes].update(data=ledGreen)
    for imageSeg in imagesSeg:
        window[imageSeg].update(data=ledGreen)

def listenScanner(window,ledGreen,ledRed,ledStart):
    import evdev
    import sys
    from evdev import InputDevice, categorize, ecodes, list_devices
    exclusive_access = 1
    device = InputDevice('/dev/input/event7')
    print(device)
    if int(exclusive_access) == 1:
        device.grab()
    letra=""
    teste=""
    cont=0
    eccoBarcodeList = []
    for event in device.read_loop():
        if event.type == evdev.ecodes.EV_KEY:
            e = categorize(event)
            if e.keystate == e.key_up:
                # if cont == 4:
                #     # print(cont)
                #     print(letra+"\n")
                #     letra=""
                #     cont=0
                # sys.stdout.write(parse_key_to_char(e.keycode))
                # cont = cont + 1
                # print(str(parse_key_to_char(e.keycode)))
                # print(cont)
                cifra = parse_key_to_char(e.keycode)
                letra = letra+str(cifra)
                # print(letra)
                # letra=""
                # cont = cont + 1
                # teste = teste+str(sys.stdout.write(parse_key_to_char(e.keycode)))
                # eccoBarcodeList.append(cifra)
                # print(eccoBarcodeList)1A

                for code in codes:
                    if(letra == code):
                        # analyzeCommand(letra)
                        updateStatus(window,ledGreen,letra)
                        letra=""
                sys.stdout.flush()
                # if(len(letra) == 2):
                #     analyzeCommand(letra)
                #     letra=""
                    
                # print(len(letra))
                # print(teste)
            if e.scancode == 28:
                print("return key detected, exiting...")
                break

def makeInterface():

    BORDER_COLOR = '#C7D5E0'
    DARK_HEADER_COLOR = '#1B2838'
    BPAD_TOP = ((20,20), (20, 10))
    BPAD_LEFT = ((20,10), (0, 10))
    BPAD_LEFT_INSIDE = (0, 10)
    BPAD_RIGHT = ((10,20), (10, 20))
    size = (25 , 25)

    top_banner = [[sg.Text('Painel de Controle'+ ' '*64, font='Any 20', background_color=DARK_HEADER_COLOR)]]
                #    sg.Text('Tuesday 9 June 2020', font='Any 20', background_color=DARK_HEADER_COLOR)]]

    top  = [[sg.Text('The Weather Will Go Here', size=(50,1), justification='c', pad=BPAD_TOP, font='Any 20')]]
                # [sg.T(f'{i*25}-{i*34}') for i in range(7)],]

    # block_3 = [[sg.Text('Block 3', font='Any 20')],
    #             [sg.Input(), sg.Text('Some Text')],
    #             [sg.Button('Go'), sg.Button('Exit')]  ]


    # block_1 = [[sg.Text('Posição 01: '),sg.Checkbox('QR-Posição',default=False, key='CBox1A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox1B'),sg.Text('Resultado: '),sg.Input(key='TextRes1'),sg.Checkbox('OK',key='CBox1C',default=False)]]
    # block_2 = [[sg.Text('Posição 02:\t QR-Posição'),sg.Image(size=(size), key='ImPos2A'),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key='ImPos2B'),sg.Text('\t\t\t\tResultado: '),sg.Input(key='TextRes2'),sg.Text('\tOK'),sg.Image(size=(size),key='ImRes2')]]
    # block_3 = [[sg.Text('Posição 03: '),sg.Checkbox('QR-Posição',default=False, key='CBox3A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox3B'),sg.Text('Resultado: '),sg.Input(key='TextRes3'),sg.Checkbox('OK',key='CBox3C',default=False)]]
    # block_4 = [[sg.Text('Posição 04: '),sg.Checkbox('QR-Posição',default=False, key='CBox4A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox4B'),sg.Text('Resultado: '),sg.Input(key='TextRes4'),sg.Checkbox('OK',key='CBox4C',default=False)]]
    # block_5 = [[sg.Text('Posição 05: '),sg.Checkbox('QR-Posição',default=False, key='CBox5A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox5B'),sg.Text('Resultado: '),sg.Input(key='TextRes5'),sg.Checkbox('OK',key='CBox5C',default=False)]]
    # block_6 = [[sg.Text('Posição 06: '),sg.Checkbox('QR-Posição',default=False, key='CBox6A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox6B'),sg.Text('Resultado: '),sg.Input(key='TextRes6'),sg.Checkbox('OK',key='CBox6C',default=False)]]
    # block_7 = [[sg.Text('Posição 07: '),sg.Checkbox('QR-Posição',default=False, key='CBox7A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox7B'),sg.Text('Resultado: '),sg.Input(key='TextRes7'),sg.Checkbox('OK',key='CBox7C',default=False)]]
    # block_8 = [[sg.Text('Posição 08: '),sg.Checkbox('QR-Posição',default=False, key='CBox8A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox8B'),sg.Text('Resultado: '),sg.Input(key='TextRes8'),sg.Checkbox('OK',key='CBox8C',default=False)]]
    # block_9 = [[sg.Text('Posição 09: '),sg.Checkbox('QR-Posição',default=False, key='CBox9A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox9B'),sg.Text('Resultado: '),sg.Input(key='TextRes9'),sg.Checkbox('OK',key='CBox9C',default=False)]]
    # block_10 = [[sg.Text('Posição 10: '),sg.Checkbox('QR-Posição',default=False, key='CBox10A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox10B'),sg.Text('Resultado: '),sg.Input(key='TextRes10'),sg.Checkbox('OK',key='CBox10C',default=False)]]
    # block_11 = [[sg.Text('Posição 11: '),sg.Checkbox('QR-Posição',default=False, key='CBox11A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox11B'),sg.Text('Resultado: '),sg.Input(key='TextRes11'),sg.Checkbox('OK',key='CBox11C',default=False)]]
    # block_12 = [[sg.Text('Posição 12: '),sg.Checkbox('QR-Posição',default=False, key='CBox12A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox12B'),sg.Text('Resultado: '),sg.Input(key='TextRes12'),sg.Checkbox('OK',key='CBox12C',default=False)]]
    # block_13 = [[sg.Text('Segurança:  '),sg.Checkbox('Operador',default=False, key='CBoxOP'),sg.Checkbox('Trava',default=False,pad=(190,0),key='CBoxTrava'),sg.Button('Executar',pad=(120,0)), sg.Button('Novo Teste',pad=(110,0))]]


    block_1 = [[sg.Text('Posição 01:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[0]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[0]),sg.Text('\tResultado: '),sg.Input(key=resultText[0]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[0])]]
    block_2 = [[sg.Text('Posição 02:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[1]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[1]),sg.Text('\tResultado: '),sg.Input(key=resultText[1]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[1])]]
    block_3 = [[sg.Text('Posição 03:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[2]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[2]),sg.Text('\tResultado: '),sg.Input(key=resultText[2]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[2])]]
    block_4 = [[sg.Text('Posição 04:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[3]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[3]),sg.Text('\tResultado: '),sg.Input(key=resultText[3]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[3])]]
    block_5 = [[sg.Text('Posição 05:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[4]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[4]),sg.Text('\tResultado: '),sg.Input(key=resultText[4]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[4])]]
    block_6 = [[sg.Text('Posição 06:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[5]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[5]),sg.Text('\tResultado: '),sg.Input(key=resultText[5]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[5])]]
    block_7 = [[sg.Text('Posição 07:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[6]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[6]),sg.Text('\tResultado: '),sg.Input(key=resultText[6]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[6])]]
    block_8 = [[sg.Text('Posição 08:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[7]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[7]),sg.Text('\tResultado: '),sg.Input(key=resultText[7]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[7])]]
    block_9 = [[sg.Text('Posição 09:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[8]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[8]),sg.Text('\tResultado: '),sg.Input(key=resultText[8]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[8])]]
    block_10 = [[sg.Text('Posição 10:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[9]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[9]),sg.Text('\tResultado: '),sg.Input(key=resultText[9]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[9])]]
    block_11 = [[sg.Text('Posição 11:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[10]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[10]),sg.Text('\tResultado: '),sg.Input(key=resultText[10]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[10])]]
    block_12 = [[sg.Text('Posição 12:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[11]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[11]),sg.Text('\tResultado: '),sg.Input(key=resultText[11]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[11])]]
    block_13 = [[sg.Text('Segurança:\t QR-Operador'),sg.Image(size=(size), key=imagesSeg[0]),sg.Text('\t   QR-Trava'),sg.Image(size=(size),key=imagesSeg[1]),sg.Text('\t\t\t      '),sg.Button('Executar'),sg.Text(''),sg.Button('Novo Teste')]]



    col = [
            #   [sg.Column(top, size=(960, 90), pad=BPAD_TOP)],
              [sg.Column([[sg.Column(block_1, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_2, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_3, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_4, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_5, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_6, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_7, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_8, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_9, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_10, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_11, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_12, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_13, size=(1010,30), pad=BPAD_LEFT_INSIDE)]])]
              ]

    block = [[sg.Text('Configurações', font='Any 20')],
            # [sg.T('This is some random text')],
            # [sg.T('This is some random text')],
            # [sg.T('This is some random text')],
            # [sg.T('This is some random text')]
    ]

    layout = [[sg.Column(top_banner, size=(1367, 70), background_color=DARK_HEADER_COLOR)],
        [sg.Column(col, size=(900,850), scrollable=True),sg.Column(block, size=(430, 850), pad=BPAD_RIGHT,scrollable=True)],
        
        # [sg.Button('Go'), sg.Button('Exit')],
        ]

    # window = sg.Window('Dashboard PySimpleGUI-Style', layout, margins=(0,0), background_color=BORDER_COLOR, no_titlebar=True, grab_anywhere=True)
    window = sg.Window('Dashboard PySimpleGUI-Style', layout, margins=(0,0), background_color=BORDER_COLOR, grab_anywhere=True,resizable=True,default_element_size=(20, 1),finalize=True)
    # window = sg.Window('Checkbox practice').Layout(layout)
    
    im = Image.open('images/ledApagado.png')
    im = im.resize(size, resample=Image.BICUBIC)
    ledStart = ImageTk.PhotoImage(image=im)

    im2 = Image.open('images/led-green.png')
    im2 = im2.resize(size, resample=Image.BICUBIC)
    ledGreen = ImageTk.PhotoImage(image=im2)

    im3 = Image.open('images/ledAceso.png')
    im3 = im3.resize(size, resample=Image.BICUBIC)
    ledRed = ImageTk.PhotoImage(image=im3)

    setLedRedAll(window,ledStart)
    
    # threadBluetooth = Thread(target = connect, args = (window,))
    # threadBluetooth.start()

    threadSerialScanner = Thread(target = listenScanner, args = (window,ledGreen,ledRed,ledStart))
    threadSerialScanner.start()
    # window.mainloop()

    while True:             # Event Loop
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Exit':
            #client.loop_stop()
            break
        
        if event == 'Executar':
            #print(client)
            # window['CBox1A'].update(True)
            # window['TextRes1'].update("423423")
            # window['ImPos2A'].update(data=image2)
            # window['ImPos2B'].update(data=image2)
            # window['ImRes2'].update(data=image2)
            # updateStatus(window,ledGreen,'ImPos1A')
            setLedGreenAll(window,ledGreen)
            # window['ImRes2'].update(data=ledRed)



        elif event == 'Novo Teste':
            setLedRedAll(window,ledStart)
    window.close()

    # size = (30 , 30)
    # im = Image.open('led.png')
    # im = im.resize(size, resample=Image.BICUBIC)
    # # im = im.resize(size, resample=Image.BICUBIC)

    # sg.theme('DarkGreen3')

    # layout = [
    #             [sg.Text('Nome:'),sg.Input(size=(40, 1))],
    #             [sg.Text('Idade:'),sg.Input(size=(20, 3))],
    #             [sg.Image(size=(50, 50), key='-IMAGE-')],
    #             [sg.Button('Send Data')]
    #         ]
    # window = sg.Window('Window Title', layout, margins=(0, 0), finalize=True)

    # # Convert im to ImageTk.PhotoImage after window finalized
    # image = ImageTk.PhotoImage(image=im)

    # # update image in sg.Image
    # window['-IMAGE-'].update(data=image)
    while True:

        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break

    window.close()

def buildInterfaceVertical():
    BORDER_COLOR = '#C7D5E0'
    DARK_HEADER_COLOR = '#1B2838'
    BPAD_TOP = ((20,20), (20, 10))
    BPAD_LEFT = ((20,10), (0, 10))
    BPAD_LEFT_INSIDE = (0, 10)
    BPAD_RIGHT = ((10,20), (10, 20))
    size = (25 , 25)

    top_banner = [[sg.Text('Painel de Controle'+ ' '*64, font='Any 20', background_color=DARK_HEADER_COLOR)]]
                #    sg.Text('Tuesday 9 June 2020', font='Any 20', background_color=DARK_HEADER_COLOR)]]

    top  = [[sg.Text('The Weather Will Go Here', size=(50,1), justification='c', pad=BPAD_TOP, font='Any 20')]]
                # [sg.T(f'{i*25}-{i*34}') for i in range(7)],]

    # block_3 = [[sg.Text('Block 3', font='Any 20')],
    #             [sg.Input(), sg.Text('Some Text')],
    #             [sg.Button('Go'), sg.Button('Exit')]  ]


    # block_1 = [[sg.Text('Posição 01: '),sg.Checkbox('QR-Posição',default=False, key='CBox1A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox1B'),sg.Text('Resultado: '),sg.Input(key='TextRes1'),sg.Checkbox('OK',key='CBox1C',default=False)]]
    # block_2 = [[sg.Text('Posição 02:\t QR-Posição'),sg.Image(size=(size), key='ImPos2A'),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key='ImPos2B'),sg.Text('\t\t\t\tResultado: '),sg.Input(key='TextRes2'),sg.Text('\tOK'),sg.Image(size=(size),key='ImRes2')]]
    # block_3 = [[sg.Text('Posição 03: '),sg.Checkbox('QR-Posição',default=False, key='CBox3A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox3B'),sg.Text('Resultado: '),sg.Input(key='TextRes3'),sg.Checkbox('OK',key='CBox3C',default=False)]]
    # block_4 = [[sg.Text('Posição 04: '),sg.Checkbox('QR-Posição',default=False, key='CBox4A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox4B'),sg.Text('Resultado: '),sg.Input(key='TextRes4'),sg.Checkbox('OK',key='CBox4C',default=False)]]
    # block_5 = [[sg.Text('Posição 05: '),sg.Checkbox('QR-Posição',default=False, key='CBox5A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox5B'),sg.Text('Resultado: '),sg.Input(key='TextRes5'),sg.Checkbox('OK',key='CBox5C',default=False)]]
    # block_6 = [[sg.Text('Posição 06: '),sg.Checkbox('QR-Posição',default=False, key='CBox6A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox6B'),sg.Text('Resultado: '),sg.Input(key='TextRes6'),sg.Checkbox('OK',key='CBox6C',default=False)]]
    # block_7 = [[sg.Text('Posição 07: '),sg.Checkbox('QR-Posição',default=False, key='CBox7A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox7B'),sg.Text('Resultado: '),sg.Input(key='TextRes7'),sg.Checkbox('OK',key='CBox7C',default=False)]]
    # block_8 = [[sg.Text('Posição 08: '),sg.Checkbox('QR-Posição',default=False, key='CBox8A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox8B'),sg.Text('Resultado: '),sg.Input(key='TextRes8'),sg.Checkbox('OK',key='CBox8C',default=False)]]
    # block_9 = [[sg.Text('Posição 09: '),sg.Checkbox('QR-Posição',default=False, key='CBox9A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox9B'),sg.Text('Resultado: '),sg.Input(key='TextRes9'),sg.Checkbox('OK',key='CBox9C',default=False)]]
    # block_10 = [[sg.Text('Posição 10: '),sg.Checkbox('QR-Posição',default=False, key='CBox10A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox10B'),sg.Text('Resultado: '),sg.Input(key='TextRes10'),sg.Checkbox('OK',key='CBox10C',default=False)]]
    # block_11 = [[sg.Text('Posição 11: '),sg.Checkbox('QR-Posição',default=False, key='CBox11A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox11B'),sg.Text('Resultado: '),sg.Input(key='TextRes11'),sg.Checkbox('OK',key='CBox11C',default=False)]]
    # block_12 = [[sg.Text('Posição 12: '),sg.Checkbox('QR-Posição',default=False, key='CBox12A'),sg.Checkbox('QR-Bancada',default=False,pad=(190,0),key='CBox12B'),sg.Text('Resultado: '),sg.Input(key='TextRes12'),sg.Checkbox('OK',key='CBox12C',default=False)]]
    # block_13 = [[sg.Text('Segurança:  '),sg.Checkbox('Operador',default=False, key='CBoxOP'),sg.Checkbox('Trava',default=False,pad=(190,0),key='CBoxTrava'),sg.Button('Executar',pad=(120,0)), sg.Button('Novo Teste',pad=(110,0))]]


    # block_1 = [[sg.Text('Posição 01:')],[sg.Text('QR-Posição')]],[sg.Image(size=(size)], key=positionImagesA[0]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[0]),sg.Text('\tResultado: '),sg.Input(key=resultText[0]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[0])]]
    block_1 = [[sg.Text('   01',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[0])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[0])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        # [sg.Multiline(size=(10, 5),key=resultText[0],font=('Courier New', 14),disabled=True)],
        [sg.Multiline(size=(10, 5),key=resultText[0],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[0])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_2 = [[sg.Text('   02',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[1])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[1])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[1],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[1])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_3 = [[sg.Text('   03',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[2])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[2])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[2],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[2])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_4 = [[sg.Text('   04',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[3])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[3])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[3],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[3])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_5 = [[sg.Text('   05',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[4])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[4])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[4],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[4])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_6 = [[sg.Text('   06',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[5])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[5])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[5],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[5])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_7 = [[sg.Text('   07',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[6])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[6])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[6],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[6])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_8 = [[sg.Text('   08',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[7])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[7])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
       [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[7],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[7])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_9 = [[sg.Text('   09',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[8])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[8])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[8],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[8])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_10 = [[sg.Text('   10',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[9])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[9])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
       [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[9],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[9])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_11 = [[sg.Text('   11',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[10])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[10])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[10],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[10])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        ]

    block_12 = [[sg.Text('   12',font='Any 20')],
        [sg.HorizontalSeparator()],
        [sg.Text('\n')],
        [sg.Text('QR-Conjunto',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesA[11])],
        [sg.Text('\n\n')],
        [sg.Text(' QR-Posição',justification='c')],
        [sg.Text('   '),sg.Image(size=(size), key=positionImagesB[11])],
        [sg.Text('\n')],
        [sg.HorizontalSeparator()],
        [sg.Text('')],
        [sg.Text('   Medição',justification='c')],
        [sg.Multiline(size=(10, 5),key=resultText[11],disabled=True)],
        [sg.Text('')],
        [sg.Text('  Resultado',justification='c')],
        [sg.Text('   '),sg.Image(size=(size),key=resultImages[11])],
        [sg.Text('\n')],
        ]

    block_13 = [[sg.Text('Segurança:\t QR-Operador'),
        sg.Image(size=(size), key=imagesSeg[0]),
        sg.Text('\t   QR-Trava'),
        sg.Image(size=(size),
        key=imagesSeg[1]),sg.Text('\t'),
        sg.Button('Executar'),sg.Text(''),sg.Button('Novo Teste')]]
    
    
    # block_2 = [[sg.Text('Posição 02:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[1]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[1]),sg.Text('\tResultado: '),sg.Input(key=resultText[1]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[1])]]
    # block_3 = [[sg.Text('Posição 03:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[2]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[2]),sg.Text('\tResultado: '),sg.Input(key=resultText[2]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[2])]]
    # block_4 = [[sg.Text('Posição 04:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[3]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[3]),sg.Text('\tResultado: '),sg.Input(key=resultText[3]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[3])]]
    # block_5 = [[sg.Text('Posição 05:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[4]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[4]),sg.Text('\tResultado: '),sg.Input(key=resultText[4]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[4])]]
    # block_6 = [[sg.Text('Posição 06:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[5]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[5]),sg.Text('\tResultado: '),sg.Input(key=resultText[5]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[5])]]
    # block_7 = [[sg.Text('Posição 07:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[6]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[6]),sg.Text('\tResultado: '),sg.Input(key=resultText[6]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[6])]]
    # block_8 = [[sg.Text('Posição 08:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[7]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[7]),sg.Text('\tResultado: '),sg.Input(key=resultText[7]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[7])]]
    # block_9 = [[sg.Text('Posição 09:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[8]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[8]),sg.Text('\tResultado: '),sg.Input(key=resultText[8]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[8])]]
    # block_10 = [[sg.Text('Posição 10:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[9]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[9]),sg.Text('\tResultado: '),sg.Input(key=resultText[9]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[9])]]
    # block_11 = [[sg.Text('Posição 11:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[10]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[10]),sg.Text('\tResultado: '),sg.Input(key=resultText[10]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[10])]]
    # block_12 = [[sg.Text('Posição 12:\t QR-Posição'),sg.Image(size=(size), key=positionImagesA[11]),sg.Text('\tQR-Bancada'),sg.Image(size=(size),key=positionImagesB[11]),sg.Text('\tResultado: '),sg.Input(key=resultText[11]),sg.Text('\tOK'),sg.Image(size=(size),key=resultImages[11])]]
    # block_13 = [[sg.Text('Segurança:\t QR-Operador'),sg.Image(size=(size), key=imagesSeg[0]),sg.Text('\t   QR-Trava'),sg.Image(size=(size),key=imagesSeg[1]),sg.Text('\t\t\t      '),sg.Button('Executar'),sg.Text(''),sg.Button('Novo Teste')]]

    block = [[sg.Text('Configurações', font='Any 20')],
            # [sg.T('This is some random text')],
            # [sg.T('This is some random text')],
            # [sg.T('This is some random text')],
            # [sg.T('This is some random text')]
    ]

    col = [
            #   [sg.Column(top, size=(960, 90), pad=BPAD_TOP)],
              [sg.Column([[sg.Column(block_1, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator(),
              sg.Column([[sg.Column(block_2, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_3, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_4, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_5, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_6, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_7, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_8, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_9, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_10, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_11, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block_12, size=(100,505), pad=BPAD_LEFT_INSIDE),sg.VerticalSeparator()]]),
              sg.Column([[sg.Column(block, size=(400,505), pad=BPAD_LEFT_INSIDE)]]),
              ]])],
              [sg.HorizontalSeparator()],
              [sg.Column([[sg.Column(block_13, size=(1900,30), pad=BPAD_LEFT_INSIDE)]])]
             
    ]



    layout = [[sg.Column(top_banner, size=(1900, 70), background_color=DARK_HEADER_COLOR)],
        [sg.Column(col, size=(1900,600), scrollable=True)],
        # [sg.Column(block, size=(1455, 300),scrollable=True)],
        
        # [sg.Button('Go'), sg.Button('Exit')],
        ]

    # window = sg.Window('Dashboard PySimpleGUI-Style', layout, margins=(0,0), background_color=BORDER_COLOR, no_titlebar=True, grab_anywhere=True)
    window = sg.Window('Dashboard PySimpleGUI-Style', layout, margins=(0,0), background_color=BORDER_COLOR, grab_anywhere=True,resizable=True,default_element_size=(12, 1),finalize=True)
    # window = sg.Window('Checkbox practice').Layout(layout)
    
    im = Image.open('images/ledApagado.png')
    im = im.resize(size, resample=Image.BICUBIC)
    ledStart = ImageTk.PhotoImage(image=im)

    im2 = Image.open('images/led-green.png')
    im2 = im2.resize(size, resample=Image.BICUBIC)
    ledGreen = ImageTk.PhotoImage(image=im2)

    im3 = Image.open('images/ledAceso.png')
    im3 = im3.resize(size, resample=Image.BICUBIC)
    ledRed = ImageTk.PhotoImage(image=im3)

    setLedRedAll(window,ledStart)
    # updateStatus(window,ledStart,positionImagesA[0])
    # updateStatus(window,ledStart,positionImagesB[0])
    # updateStatus(window,ledStart,resultImages[0])

    # threadBluetooth = Thread(target = connect, args = (window,))
    # threadBluetooth.start()

    threadSerialScanner = Thread(target = listenScanner, args = (window,ledGreen,ledRed,ledStart))
    threadSerialScanner.start()
    # window.mainloop()

    while True:             # Event Loop
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Exit':
            #client.loop_stop()
            break
        
        if event == 'Executar':
            #print(client)
            # window['CBox1A'].update(True)
            # window['TextRes1'].update("423423")
            # window['ImPos2A'].update(data=image2)
            # window['ImPos2B'].update(data=image2)
            # window['ImRes2'].update(data=image2)
            # updateStatus(window,ledGreen,'ImPos1A')
            setLedGreenAll(window,ledGreen)
            # window['ImRes2'].update(data=ledRed)



        elif event == 'Novo Teste':
            setLedRedAll(window,ledStart)
    window.close()

    # size = (30 , 30)
    # im = Image.open('led.png')
    # im = im.resize(size, resample=Image.BICUBIC)
    # # im = im.resize(size, resample=Image.BICUBIC)

    # sg.theme('DarkGreen3')

    # layout = [
    #             [sg.Text('Nome:'),sg.Input(size=(40, 1))],
    #             [sg.Text('Idade:'),sg.Input(size=(20, 3))],
    #             [sg.Image(size=(50, 50), key='-IMAGE-')],
    #             [sg.Button('Send Data')]
    #         ]
    # window = sg.Window('Window Title', layout, margins=(0, 0), finalize=True)

    # # Convert im to ImageTk.PhotoImage after window finalized
    # image = ImageTk.PhotoImage(image=im)

    # # update image in sg.Image
    # window['-IMAGE-'].update(data=image)
    while True:

        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break

    window.close()

def connect(window):
    while True:
        server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        port = 1
        server_sock.bind(("",port))
        server_sock.listen(1)

        client_sock,address = server_sock.accept()
        print ("Accepted connection from ",address)

        data = client_sock.recv(1024)
        print ("received [%s]" % data)
        
        jsonRecebido = str((data))
        jsonCerto = jsonRecebido[2:len(jsonRecebido)-1]
        
        jsonQ = json.loads(jsonCerto)
        element = jsonQ.get("element")
        text = jsonQ.get("text")
        
        window[str(element)].update(True)
        window[str(text)].update("Texto Recebido")	

#connectMQTT()
# threadInterface = Thread(target = makeInterface, args = ())
# threadInterface.start()    
threadInterfaceVertical = Thread(target = buildInterfaceVertical, args = ())
threadInterfaceVertical.start()    
#threadBluetooth = Thread(target = connect, args = ())
#threadBluetooth.start()
